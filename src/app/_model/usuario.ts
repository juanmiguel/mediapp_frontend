import { Rol } from './rol';

export class Usuario{
    idUsuario: number;
    password: string;
    username: string;
    enabled: boolean;
    roles: Rol[];
}