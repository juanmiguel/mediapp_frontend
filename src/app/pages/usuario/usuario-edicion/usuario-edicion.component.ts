import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Usuario } from 'src/app/_model/usuario';
import { UsuarioService } from 'src/app/_service/usuario.service';
import { switchMap } from 'rxjs/operators';
import { Rol } from 'src/app/_model/rol';
import { RolService } from 'src/app/_service/rol.service';

@Component({
  selector: 'app-usuario-edicion',
  templateUrl: './usuario-edicion.component.html',
  styleUrls: ['./usuario-edicion.component.css']
})
export class UsuarioEdicionComponent implements OnInit {

  form: FormGroup = new FormGroup({});
  id: number = 0;
  edicion: boolean = false;

  roles: Rol[];
  roldet: Rol[] = [];
  selectedObjectsFromArray: any = [];

  constructor(
    private route: ActivatedRoute,
    private usuarioService: UsuarioService,
    private rolService: RolService,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.form = new FormGroup({
      'id': new FormControl(0),
      'username': new FormControl(''),
      'password': new FormControl(''),
    });

    this.route.params.subscribe((data: Params) => {
      this.id = data['id'];
      this.edicion = data['id'] != null;
      this.initForm();
    });

    this.listarRoles();

  }

  initForm() {
    if (this.edicion) {
      this.usuarioService.listarPorId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'id': new FormControl(data.idUsuario),
          'username': new FormControl(data.username),
          'password': new FormControl(data.password),
        });
      });
      this.listarPorId(this.id);
    }
  }

  operar() {
    let usuario = new Usuario();
    usuario.idUsuario = this.form.value['id'];
    usuario.username = this.form.value['username'];
    usuario.password = this.form.value['password'];
    usuario.enabled = true;
    usuario.roles = [];

    for (let i = 0; i < this.selectedObjectsFromArray.length; i++) {
      let item = this.selectedObjectsFromArray[i];
      let det = new Rol();

      det.idRol = item.idRol;
      det.nombre = item.nombre;

      this.roldet.push(det);
    }
    usuario.roles = this.roldet;

    if (this.edicion) {
      this.usuarioService.modificar(usuario).subscribe(() => {
        this.usuarioService.listar().subscribe(data => {
          this.usuarioService.setUsuarioCambio(data);
          this.usuarioService.setMensajeCambio('SE MODIFICO');
        });
      });
    } else {
      this.usuarioService.registrar(usuario).pipe(switchMap(() => {
        return this.usuarioService.listar();
      }))
      .subscribe(data => {
        this.usuarioService.setUsuarioCambio(data);
        this.usuarioService.setMensajeCambio('SE REGISTRO');
      });
    }

    this.router.navigate(['/pages/usuario']);
  }

  listarRoles() {
    this.rolService.listar().subscribe(data => {
      this.roles = data;
    });
  }

  listarPorId(id : number){
    this.usuarioService.listarPorId(id).subscribe(data => {
      this.selectedObjectsFromArray = data['roles'];
    });
  }

  compareFnRol(a: Rol, b: Rol) {
    return a && b ? a.idRol === b.idRol : a === b;
  }

}
