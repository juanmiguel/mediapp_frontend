import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Menu } from 'src/app/_model/menu';
import { MenuService } from 'src/app/_service/menu.service';
import { switchMap } from 'rxjs/operators';
import { Rol } from 'src/app/_model/rol';
import { RolService } from 'src/app/_service/rol.service';


@Component({
  selector: 'app-menu-edicion',
  templateUrl: './menu-edicion.component.html',
  styleUrls: ['./menu-edicion.component.css']
})
export class MenuEdicionComponent implements OnInit {

  form: FormGroup = new FormGroup({});
  id: number = 0;
  edicion: boolean = false;

  roles: Rol[];
  roldet: Rol[] = [];
  selectedObjectsFromArray: any = [];

  constructor(
    private route: ActivatedRoute,
    private menuService: MenuService,
    private rolService: RolService,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombre': new FormControl(''),
      'icono': new FormControl(''),
      'url': new FormControl(''),
    });

    this.route.params.subscribe((data: Params) => {
      this.id = data['id'];
      this.edicion = data['id'] != null;
      this.initForm();
    });

    this.listarRoles();
  }


  initForm() {
    if (this.edicion) {
      this.menuService.listarPorId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'id': new FormControl(data.idMenu),
          'nombre': new FormControl(data.nombre),
          'icono': new FormControl(data.icono),
          'url': new FormControl(data.url),
        });
      });
      this.listarPorId(this.id);
    }
  }

  operar() {
    let menu = new Menu();
    menu.idMenu = this.form.value['id'];
    menu.nombre = this.form.value['nombre'];
    menu.icono = this.form.value['icono'];
    menu.url = this.form.value['url'];
    menu.roles = [];

    for (let i = 0; i < this.selectedObjectsFromArray.length; i++) {
      let item = this.selectedObjectsFromArray[i];
      let det = new Rol();

      det.idRol = item.idRol;
      det.nombre = item.nombre;

      this.roldet.push(det);
    }
    menu.roles = this.roldet;

    if (this.edicion) {
      this.menuService.modificar(menu).subscribe(() => {
        this.menuService.listar().subscribe(data => {
          this.menuService.setMenuCambio(data);
          this.menuService.setMensajeCambio('SE MODIFICO');
        });
      });
    } else {
      this.menuService.registrar(menu).pipe(switchMap(() => {
        return this.menuService.listar();
      }))
      .subscribe(data => {
        this.menuService.setMenuCambio(data);
        this.menuService.setMensajeCambio('SE REGISTRO');
      });
    }

    this.router.navigate(['/pages/menu']);
  }

  listarRoles() {
    this.rolService.listar().subscribe(data => {
      this.roles = data;
    });
  }

  listarPorId(id : number){
    this.menuService.listarPorId(id).subscribe(data => {
      this.selectedObjectsFromArray = data['roles'];
    });
  }

  compareFnRol(a: Rol, b: Rol) {
    return a && b ? a.idRol === b.idRol : a === b;
  }

}