import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Rol } from 'src/app/_model/rol';
import { RolService } from 'src/app/_service/rol.service';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-rol-edicion',
  templateUrl: './rol-edicion.component.html',
  styleUrls: ['./rol-edicion.component.css']
})
export class RolEdicionComponent implements OnInit {

  form: FormGroup = new FormGroup({});
  id: number = 0;
  edicion: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private rolService: RolService,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombre': new FormControl(''),
    });

    this.route.params.subscribe((data: Params) => {
      this.id = data['id'];
      this.edicion = data['id'] != null;
      this.initForm();
    });

  }

  initForm() {
    if (this.edicion) {
      this.rolService.listarPorId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'id': new FormControl(data.idRol),
          'nombre': new FormControl(data.nombre),
        });
      });
    }
  }

  operar() {
    let rol = new Rol();
    rol.idRol = this.form.value['id'];
    rol.nombre = this.form.value['nombre'];

    if (this.edicion) {
      this.rolService.modificar(rol).subscribe(() => {
        this.rolService.listar().subscribe(data => {
          this.rolService.setRolCambio(data);
          this.rolService.setMensajeCambio('SE MODIFICO');
        });
      });
    } else {
      this.rolService.registrar(rol).pipe(switchMap(() => {
        return this.rolService.listar();
      }))
      .subscribe(data => {
        this.rolService.setRolCambio(data);
        this.rolService.setMensajeCambio('SE REGISTRO');
      });
    }

    this.router.navigate(['/pages/rol']);
  }

}
